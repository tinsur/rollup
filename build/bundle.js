
(function(l, r) { if (!l || l.getElementById('livereloadscript')) return; r = l.createElement('script'); r.async = 1; r.src = '//' + (self.location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1'; r.id = 'livereloadscript'; l.getElementsByTagName('head')[0].appendChild(r) })(self.document);
'use strict';

var e = [],
  t = [];
function n(n, r) {
  if (n && "undefined" != typeof document) {
    var a,
      s = !0 === r.prepend ? "prepend" : "append",
      d = !0 === r.singleTag,
      i = "string" == typeof r.container ? document.querySelector(r.container) : document.getElementsByTagName("head")[0];
    if (d) {
      var u = e.indexOf(i);
      -1 === u && (u = e.push(i) - 1, t[u] = {}), a = t[u] && t[u][s] ? t[u][s] : t[u][s] = c();
    } else a = c();
    65279 === n.charCodeAt(0) && (n = n.substring(1)), a.styleSheet ? a.styleSheet.cssText += n : a.appendChild(document.createTextNode(n));
  }
  function c() {
    var e = document.createElement("style");
    if (e.setAttribute("type", "text/css"), r.attributes) for (var t = Object.keys(r.attributes), n = 0; n < t.length; n++) e.setAttribute(t[n], r.attributes[t[n]]);
    var a = "prepend" === s ? "afterbegin" : "beforeend";
    return i.insertAdjacentElement(a, e), e;
  }
}

var css = "body {\r\n  background-color: darkred;\r\n  color: white;\r\n  margin: 0px;\r\n  padding: 0;\r\n}\r\nimg{\r\n  max-width: 500px;\r\n  display: block;\r\n  margin: 0 auto;\r\n}\r\n\r\n.title{\r\n  margin-bottom: 50px;\r\n  text-align: center;\r\n}";
n(css,{});

function hello() {
  console.log("Hello world!");
}
hello();
var array = [1, 2, 3].map(function (n) {
  return n + 1;
});
console.log(array);
var titleOne = document.createElement('h1');
console.log(titleOne);
titleOne.className = 'title';
titleOne.textContent = 'I love JavaScript';
document.body.append(titleOne);
var img = document.createElement("img");
img.src = './assets/jslogo.png';
document.body.append(img);
