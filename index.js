import "./index.css";

function hello() {
  console.log("Hello world!");
}

hello();

const array = [1, 2, 3].map(n => n + 1);
console.log(array);

import "./index.css";

const titleOne = document.createElement('h1')
console.log(titleOne);
titleOne.className='title'
titleOne.textContent='I love JavaScript'
document.body.append(titleOne);

const img = document.createElement("img");
img.src = './assets/jslogo.png';
document.body.append(img);