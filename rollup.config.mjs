import resolve from '@rollup/plugin-node-resolve';//* плагин подключениявнешних модулей*//
import babel from '@rollup/plugin-babel';//*плагин для конвертации версий js*//
import styles from "rollup-plugin-styles";//*плагин для подключения стилей*//
import image from '@rollup/plugin-image';//*плагин для подключение картинок*//
import serve from 'rollup-plugin-serve'//*плагин для работы на локальном серевере*//
import livereload from 'rollup-plugin-livereload'//*отслеживание изменений*//

export default {
  input: './index.js',
  output: {
    file: './build/bundle.js',
    format: 'cjs'
  },
  plugins: [resolve(), babel({ babelHelpers: 'bundled' }), styles(), image(), serve({ open: true }), livereload()]
};